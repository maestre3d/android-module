/*
 * Autor: Alonso Ruiz
 * Versión: 0.5b;
 * Descripción: Clase controlador para el fragmento Home.
 * Fecha: 02/10/2019
 * */

package com.damascus.atomus;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.damascus.atomus.Adapters.EmptyRecyclerView;
import com.damascus.atomus.Adapters.HomeAdapter;
import com.damascus.atomus.Models.Practice;
import com.damascus.atomus.Remote.RetrofitClient;
import com.damascus.atomus.Services.Global;
import com.damascus.atomus.Services.UserService;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;


public class HomeFragment extends Fragment {

    // Import userService for API calling
    UserService userService;
    private static ArrayList<Practice> practices = null;

    // UI/UX Components
    View v;
    EmptyRecyclerView rvPractices;
    SwipeRefreshLayout mySwipeRefreshLayout;
    LinearLayout holder;
    TextView empty_view;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_home, container, false);

        // Set window name
        CustomGlobal.window = "HOME";

        // Load UI/UX components
        TextView user_name = (TextView) v.findViewById(R.id.home_user);
        TextView user_mat = (TextView) v.findViewById(R.id.home_mat);
        holder = (LinearLayout) v.findViewById(R.id.holder_h);
        empty_view = (TextView) v.findViewById(R.id.home_nodata);
        mySwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.frame_home);

        // Set UI data
        user_mat.setText(Global.IDENTITY.getUsername());
        user_name.setText(Global.IDENTITY.getName());
        CircleImageView user_pic = (CircleImageView) v.findViewById(R.id.home_pic);

        // Download and set user_pic
        Picasso.with(v.getContext())
                .load(Global.PIC_URI + Global.IDENTITY.getImage())
                .placeholder(v.getResources().getDrawable(R.drawable.generic_user, v.getContext().getTheme()))
                .error(v.getResources().getDrawable(R.drawable.generic_user,  v.getContext().getTheme()))
                .resize(500, 500)
                .centerCrop()
                .into(user_pic);


        // Get recycler from R
        //EmptyRecyclerView recyclerView = (EmptyRecyclerView) v.findViewById(R.id.home_cards);
        rvPractices = (EmptyRecyclerView) v.findViewById(R.id.home_cards);

        // Create linear manager to manage recycler's layout
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvPractices.setEmptyView(holder);


        userService = new UserService();
        //userService.refreshIdentity(v);
        if(Global.IDENTITY != null){
            userService.refreshFeed(v);
        }


        /* Get custom adapter
        HomeAdapter adapter = new HomeAdapter(practices);*/
        rvPractices.setHasFixedSize(true);

        // Set layout manager and set adapter
        rvPractices.setLayoutManager(linearLayoutManager);

        // Add dividers
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvPractices.getContext(), linearLayoutManager.getOrientation());
        rvPractices.addItemDecoration(dividerItemDecoration);

        // Add recycler anims
        rvPractices.setItemAnimator(new DefaultItemAnimator());

        // SwipeRefresh listener
        //  Refreshes identity with home with one swipe
        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {

                        // This method performs the actual data-refresh operation.
                        // The method calls setRefreshing(false) when it's finished.
                        userService.refreshIdentity(v);
                        Picasso.with(v.getContext())
                                .load(Global.PIC_URI + Global.IDENTITY.getImage())
                                .placeholder(v.getResources().getDrawable(R.drawable.generic_user, v.getContext().getTheme()))
                                .error(v.getResources().getDrawable(R.drawable.generic_user,  v.getContext().getTheme()))
                                .resize(500, 500)
                                .centerCrop()
                                .into(user_pic);
                        userService.refreshFeed(v);
                        userService.getActiveOrd(v);
                        userService.getExpOrders(v);
                    }
                }
        );


        // Inflate the layout for this fragment
        return v;
    }

    @Override
    public void onStart(){
        super.onStart();
        empty_view.setVisibility(View.GONE);
    }

    @Override
    public void onResume(){
        super.onResume();

        userService.getActiveOrd(v);
        userService.getExpOrders(v);
    }

    public void setPractices(ArrayList<Practice> practices) {
        this.practices = practices;
    }


}
