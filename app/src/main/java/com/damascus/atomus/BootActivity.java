/*
 * Autor: Alonso Ruiz
 * Versión: 0.5b;
 * Descripción: Clase controlador de la actividad Boot.
 * Fecha: 02/10/2019
 * */

package com.damascus.atomus;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;

import com.damascus.atomus.Services.Global;
import com.damascus.atomus.Services.JwtService;
import com.damascus.atomus.Services.UserService;
import com.google.firebase.FirebaseApp;


public class BootActivity extends AppCompatActivity {

    ProgressBar spinner;

    private final int BOOT_DURATION = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boot);

        spinner = (ProgressBar) findViewById(R.id.boot_spinner);


        spinner.getIndeterminateDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

        new Handler().postDelayed(new Runnable() {
            public void run() {
                verifyToken();
            };
        }, BOOT_DURATION);
    }

    private void verifyToken(){
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("Credentials", 0);
        String token = preferences.getString("TOKEN", null);
        if(token == null){
            Intent intent = new Intent(getApplicationContext(), LogActivity.class);
            startActivity(intent);
            finish();
        }else{
            JwtService jwtService = new JwtService(token);
            if (jwtService.checkExp()){
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
            }else{
                UserService userService = new UserService();
                userService.unsetDeviceFCM();
                Global.IDENTITY = null;
                Global.TOKEN = null;
                Intent intent = new Intent(getApplicationContext(), LogActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }
}
