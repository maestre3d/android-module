/*
 * Autor: Alonso Ruiz
 * Versión: 0.5b;
 * Descripción: Clase modelo para la colección Order.
 * Fecha: 02/10/2019
 * */


package com.damascus.atomus.Models;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Order {

    @SerializedName("items")
    @Expose
    private ArrayList<Item> items = null;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("iat")
    @Expose
    private Integer iat;
    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("practice")
    @Expose
    private Practice practice;
    @SerializedName("exp")
    @Expose
    private Integer exp;

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getIat() {
        return iat;
    }

    public void setIat(Integer iat) {
        this.iat = iat;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Practice getPractice() {
        return practice;
    }

    public void setPractice(Practice practice) {
        this.practice = practice;
    }

    public Integer getExp() {
        return exp;
    }

    public void setExp(Integer exp) {
        this.exp = exp;
    }

}