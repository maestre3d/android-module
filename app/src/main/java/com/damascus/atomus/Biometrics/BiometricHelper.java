package com.damascus.atomus.Biometrics;

import android.content.Context;

import com.damascus.atomus.Biometrics.Callbacks.HelperCallback;

public interface BiometricHelper {

    void encodePin(Context context, String pin, HelperCallback<String> callBack);

    void delete(HelperCallback<Boolean> callback);

    void isPinCodeEncryptionKeyExist(HelperCallback<Boolean> callback);

}
