/*
 * Autor: Alonso Ruiz
 * Versión: 0.5b;
 * Descripción: Clase controlador para la actividad Order.
 * Fecha: 02/10/2019
 * */

package com.damascus.atomus;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.damascus.atomus.Models.Order;
import com.damascus.atomus.Services.UserService;


import java.text.SimpleDateFormat;
import java.util.Locale;

public class OrderActivity extends AppCompatActivity {

    // Materials listview
    ListView listView;

    // Params
    static Order order = null;
    TextView pTx, mTx, teacherN, quantityN, statusN, iatN, expN;

    // Time Formatter
    SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMMM hh:mm a", Locale.getDefault());
    java.util.Date time, exp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set activity transition
        overridePendingTransition(R.anim.slidein_right, R.anim.slideout_left);


        setContentView(R.layout.activity_order);
        CustomGlobal.isHome = false;
        UserService userService = new UserService();
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        // Load UI/UX components
        pTx = (TextView) findViewById(R.id.dorder_in3);
        mTx = (TextView) findViewById(R.id.dorder_in1);
        teacherN = (TextView) findViewById(R.id.dorder_in2);
        quantityN = (TextView) findViewById(R.id.dorder_in4);
        statusN = (TextView) findViewById(R.id.dorder_in5);
        listView = (ListView) findViewById(R.id.dorder_list);
        iatN = (TextView) findViewById(R.id.dorder_datetx);
        expN = (TextView) findViewById(R.id.dorder_datetx2);
        time = new java.util.Date((long)order.getIat()*1000);
        exp = new java.util.Date((long)order.getExp()*1000);
        Button cancelBtn = (Button) findViewById(R.id.dorder_button);
        cancelBtn.setVisibility(View.GONE);

        // Create a materials set once

        // Create an list adapter to insert using android's default list layout
        // Edited toString() method on model to get name from object
        ArrayAdapter adapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_2, android.R.id.text1, order.getItems()) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                TextView text2 = (TextView) view.findViewById(android.R.id.text2);

                text2.setTextSize(12);
                text2.setTextColor(Color.GRAY);

                text1.setText(order.getItems().get(position).getMaterial().getName());
                text2.setText("Cantidad: " + order.getItems().get(position).getQuantity().toString());
                return view;
            }
        };
        listView.setAdapter(adapter);

        // Set UI/UX data
        pTx.setText(order.getPractice().getName());
        mTx.setText(order.getPractice().getCourse().getName());
        teacherN.setText(order.getPractice().getCourse().getTeacher().getName() + " " + order.getPractice().getCourse().getTeacher().getSurname());
        statusN.setText(order.getStatus());
        statusN.setTextColor(setState(order.getStatus()));
        quantityN.setText(Integer.toString(getTotal()));
        iatN.setText(dateFormat.format(time));
        expN.setText(dateFormat.format(exp));
        getSupportActionBar().setTitle("Orden de " + order.getPractice().getName());

        // listview listeners
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View v,int position, long arg3)
            {
                Toast.makeText(OrderActivity.this, order.getItems().get(position).getMaterial().getName(), Toast.LENGTH_SHORT).show();
            }
        });


        // Button listener
        if(!CustomGlobal.isExpired){
            cancelBtn.setVisibility(View.VISIBLE);
            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext(), R.style.MyDialogTheme);

                    builder.setMessage("Para confirmar la cancelación de su pedido, favor de presionar Aceptar.")
                            .setTitle("¿Cancelar Orden?")
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    userService.rmOrder(v, order.getId());
                                    userService.refreshFeed(v);
                                    finish();
                                    overridePendingTransition(R.anim.slidein_left, R.anim.slideout_right);
                                }
                            })
                            .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            });
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slidein_left, R.anim.slideout_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    // Current order's setter
    public void setOrder(Order order){ this.order = order;}

    // Get total items
    private Integer getTotal(){
        int x = 0;
        for (int i = 0; i < order.getItems().size(); i++){
            x = x + order.getItems().get(i).getQuantity();
        }

        return x;
    }

    // Set STATE's text color depending current order state
    private Integer setState(String x){
        switch (x){
            case "PENDIENTE":
                return Color.rgb(66, 133, 244);

            case "OK":
                return Color.rgb(0, 122, 8);

            case "LISTO":
                return Color.rgb(0, 122, 8);

            case "SIN ENTREGAR":
                return Color.rgb(255, 193, 7);

            case "ENTREGADO":
                return Color.rgb(0, 122, 8);

            case "NO ENTREGADO":
                return Color.rgb(202, 0, 0);

            default:
                return Color.GRAY;
        }
    }
}
