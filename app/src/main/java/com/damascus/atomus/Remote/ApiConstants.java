package com.damascus.atomus.Remote;

public class ApiConstants {
    public static final String URL_LOGIN = "login";
    public static final String URL_USER = "user/";
    public static final String URL_HOME = "home";
    public static final String URL_ORDER = "order";
    public static final String URL_ACTIVE = "user/order/1";
    public static final String URL_EXPIRED = "user/order-exp/1";
    public static final String URL_DEVICE = "auth";
}
