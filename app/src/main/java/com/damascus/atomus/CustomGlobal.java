/*
 * Autor: Alonso Ruiz
 * Versión: 0.5b;
 * Descripción: Clase auxiliar que contiene constantes globales. (Variables públicas estáticas).
 * Fecha: 02/10/2019
 * */

package com.damascus.atomus;

import com.damascus.atomus.Models.Order;

import java.util.ArrayList;

public class CustomGlobal {
    public static String window = "HOME";
    public static Boolean isHome = false;
    public static boolean flag = false;
    public static boolean isCourse = false;
    public static boolean isBooted = true;
    public static boolean isExpired = false;

    // Genarate an string array
    public static ArrayList<Order> orders = new ArrayList<Order>();
    public static ArrayList<Order> exp_ords = new ArrayList<Order>();
}
