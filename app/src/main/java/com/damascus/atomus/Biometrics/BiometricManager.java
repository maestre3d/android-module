package com.damascus.atomus.Biometrics;

public class BiometricManager {
    private static final BiometricManager ourInstance = new BiometricManager();

    public static BiometricManager getInstance() {
        return ourInstance;
    }

    private BiometricManager() {
    }

    private BiometricHelper mPinCodeHelper = FingerprintHelper.getInstance();

    public void setPinCodeHelper(BiometricHelper pinCodeHelper) {
        mPinCodeHelper = pinCodeHelper;
    }

    public BiometricHelper getPinCodeHelper() {
        return mPinCodeHelper;
    }
}
