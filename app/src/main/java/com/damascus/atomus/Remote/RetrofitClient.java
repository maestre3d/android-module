package com.damascus.atomus.Remote;

import com.damascus.atomus.Services.Global;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static APIService API_SERVICE;

    public static APIService getClient() {
        if (API_SERVICE == null) {
            Retrofit adapter = new Retrofit.Builder()
                    .baseUrl(Global.BASE_URI)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
            API_SERVICE = adapter.create(APIService.class);
        }
        return API_SERVICE;
    }

    public static Observable<Response<JsonObject>> logInUser(String x, String y) {
        return getClient().logIn(x, y, "true");
    }

    public static Observable<Response<JsonObject>> getUser(String x, String y){
        // x = user_id; y = token;
        return getClient().getUser(x, y);
    }

    public static Observable<Response<JsonArray>> getFeed(String x){
        // x = token;
        return getClient().getFeed(x);
    }

    public static Observable<Response<JsonObject>> makeOrder(String x, String y){
        // x = practice_id; y = token;
        return getClient().makeOrder(x, y);
    }

    public static Observable<Response<JsonObject>> getOrder(String x, String y){
        // x = order_id; y = token;
        return getClient().getOrder(x, y);
    }

    public static Observable<Response<JsonObject>> removeOrder(String id, String token){
        return getClient().removeOrder(id, token);
    }

    public static Observable<Response<JsonArray>> getActive(String x){
        // x = token;
        return getClient().getActiveOrders(x);
    }

    public static Observable<Response<JsonArray>> getExp(String x){
        // x = token;
        return getClient().getExpOrders(x);
    }

    public static Observable<Response<JsonObject>> setDevice(String deviceID, String token){
        return getClient().setDevice(deviceID, token);
    }

    public static Observable<Response<JsonObject>> unsetDevice(String token){
        return getClient().unsetDevice(token);
    }

}
