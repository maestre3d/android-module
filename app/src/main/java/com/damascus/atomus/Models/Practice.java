/*
 * Autor: Alonso Ruiz
 * Versión: 0.5b;
 * Descripción: Clase modelo para la colección Practice.
 * Fecha: 02/10/2019
 * */

package com.damascus.atomus.Models;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Practice {

    @SerializedName("items")
    @Expose
    private ArrayList<Item> items = null;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("postDate")
    @Expose
    private String postDate;
    @SerializedName("expDate")
    @Expose
    private Integer expDate;
    @SerializedName("file")
    @Expose
    private String file;
    @SerializedName("course")
    @Expose
    private Course course;

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public Integer getExpDate() {
        return expDate;
    }

    public void setExpDate(Integer expDate) {
        this.expDate = expDate;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

}