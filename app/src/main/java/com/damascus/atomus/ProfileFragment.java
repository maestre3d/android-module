/*
 * Autor: Alonso Ruiz
 * Versión: 0.5b;
 * Descripción: Clase controlador para el fragmento Profile.
 * Fecha: 02/10/2019
 * */

package com.damascus.atomus;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.damascus.atomus.Adapters.ProfileAdapter;
import com.damascus.atomus.Services.Global;
import com.damascus.atomus.Services.UserService;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends Fragment implements View.OnClickListener {

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    View v;

    UserService userService;


    Button btnLogout;
    Button btnBrowser;
    CircleImageView user_pic;
    TextView user_name, user_mat, user_grade;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Set window name
        CustomGlobal.window = "PROFILE";
        CustomGlobal.isHome = true;

        v = inflater.inflate(R.layout.fragment_profile, container, false);
        userService = new UserService();

        // Get recycler from R
        RecyclerView rvProfile = (RecyclerView) v.findViewById(R.id.profile_list);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());


        // Declare components
        btnLogout = (Button) v.findViewById(R.id.profile_logut);
        btnBrowser = (Button) v.findViewById(R.id.profile_bt1);
        TextView course_txt = (TextView) v.findViewById(R.id.profile_tx1);
        user_pic = (CircleImageView) v.findViewById(R.id.profile_pic);
        user_name = (TextView) v.findViewById(R.id.profile_name);
        user_mat = (TextView) v.findViewById(R.id.profile_mat);
        user_grade = (TextView) v.findViewById(R.id.profile_grade);


        preferences = v.getContext().getSharedPreferences("Credentials", 0);
        editor = preferences.edit();
        course_txt.setVisibility(v.GONE);

        // Load contents
        loadProfile();
        if(Global.IDENTITY.getCourse() != null){
            if(!Global.IDENTITY.getCourse().isEmpty()){
                course_txt.setVisibility(v.VISIBLE);
                ProfileAdapter adapter = new ProfileAdapter(Global.IDENTITY.getCourse());
                rvProfile.setHasFixedSize(true);

                rvProfile.setLayoutManager(linearLayoutManager);
                rvProfile.setAdapter(adapter);
            }
        }

        btnLogout.setOnClickListener(this);
        btnBrowser.setOnClickListener(this);


        // Inflate the layout for this fragment
        return v;
    }

    private void loadProfile(){
        Picasso.with(v.getContext())
                .load(Global.PIC_URI + Global.IDENTITY.getImage())
                .placeholder(v.getResources().getDrawable(R.drawable.generic_user, v.getContext().getTheme()))
                .error(v.getResources().getDrawable(R.drawable.generic_user,  v.getContext().getTheme()))
                .resize(450, 450)
                .centerCrop()
                .into(user_pic);

        user_name.setText(Global.IDENTITY.getName() + " " + Global.IDENTITY.getSurname());
        user_mat.setText(Global.IDENTITY.getUsername());
        user_grade.setText("Grado: " + Global.IDENTITY.getGrade().toString() + " Semestre");
    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.profile_logut:
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);

                builder.setMessage("Para cerrar su sesión, favor de presionar el botón Aceptar.")
                        .setTitle("¿Desea cerrar sesión?")
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                userService.unsetDeviceFCM();
                                CustomGlobal.orders.clear();
                                CustomGlobal.exp_ords.clear();
                                Global.TOKEN = null;
                                Global.IDENTITY = null;
                                editor.clear();
                                editor.commit();
                                CustomGlobal.isCourse = false;
                                if (CustomGlobal.isBooted){
                                    Intent intent = new Intent(getContext(), LogActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                }else {
                                    getActivity().finish();
                                }
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).show();
                break;
            case R.id.profile_bt1:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://fcq.uach.mx/"));
                startActivity(browserIntent);
                break;
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        loadProfile();
    }


}
