package com.damascus.atomus.Biometrics;

import android.content.Context;
import android.support.annotation.NonNull;

public interface IBiometricUtils {
    String encode(@NonNull Context context, String alias, String input, boolean isAuthorizationRequared)
            throws BiometricException ;

    String decode(String alias, String encodedString) throws BiometricException;

    boolean isKeystoreContainAlias(String alias) throws BiometricException;

    void deleteKey(String alias) throws BiometricException;
}
