package com.damascus.atomus.Services;

import com.damascus.atomus.Models.Practice;
import com.damascus.atomus.Models.User;

import java.util.ArrayList;

public class Global {
    public static final String BASE_URI = "http://189.237.153.114:3979/api/";
    public static final String PIC_URI = BASE_URI + "user/pic/";
    public static String TOKEN;
    public static User IDENTITY;

    public static ArrayList<Practice> FEED = null;
    public static String FILE_URI = BASE_URI + "/practice/file/";

    // Get UserRouter's client
    /*public static APIService getAPIService() {
        return RetrofitClient.getClient().create(APIService.class);
    }*/

}
