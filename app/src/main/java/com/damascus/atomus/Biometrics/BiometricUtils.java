package com.damascus.atomus.Biometrics;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.annotation.NonNull;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.util.Base64;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.MGF1ParameterSpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.OAEPParameterSpec;
import javax.crypto.spec.PSource;

public class BiometricUtils implements IBiometricUtils {

    private String keystoreAlias = "fingerAuth";

    private static final BiometricUtils ourInstance = new BiometricUtils();

    public static BiometricUtils getInstance(){
        return ourInstance;
    }

    private BiometricUtils(){

    }

    public static boolean isFingerprintAuthAvailable(Context context){
        if(!FingerprintManagerCompat.from(context).isHardwareDetected()){
            return false;
        }

        if(!FingerprintManagerCompat.from(context).hasEnrolledFingerprints()){
            return false;
        }

        return true;
    }
    // => 1
    private KeyStore loadKeyStore() throws BiometricException {
        try {
            final KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);
            return keyStore;
        } catch (KeyStoreException
                | NoSuchAlgorithmException
                | CertificateException
                | IOException e) {
            e.printStackTrace();
            throw new BiometricException(
                    "Can not load keystore:" + e.getMessage(),
                    BiometricErrorCodes.ERROR_LOAD_KEY_STORE
            );
        }
    }

    // => 2
    @TargetApi(Build.VERSION_CODES.M)
    private boolean generateKey( String key ,boolean isAuthenticationRequired )  {
        try {
            final KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance(
                    KeyProperties.KEY_ALGORITHM_RSA, "AndroidKeyStore");
            keyGenerator.initialize(
                    new KeyGenParameterSpec.Builder(key,
                            KeyProperties.PURPOSE_ENCRYPT |
                                    KeyProperties.PURPOSE_DECRYPT)
                            .setDigests(KeyProperties.DIGEST_SHA256, KeyProperties.DIGEST_SHA512)
                            .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_OAEP)
                            .setUserAuthenticationRequired(isAuthenticationRequired)
                            .build());
            keyGenerator.generateKeyPair();
            return true;

        } catch ( NoSuchAlgorithmException
                | NoSuchProviderException
                | InvalidAlgorithmParameterException exc) {
            exc.printStackTrace();
            return false;
        }
    }

    private boolean generateKeyIfNecessary( String key , KeyStore keyStore, boolean isAuthenticationRequired) {
        try {
            return keyStore.containsAlias(key) || generateKey( key ,isAuthenticationRequired);
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
        return false;
    }

    /*
    private boolean generateKey(Context context, String keystoreAlias, boolean isAuthenticationRequired) {
        return generateKey(keystoreAlias, isAuthenticationRequired);
    }*/

    // => 3
    private Cipher getCipherInstance() throws BiometricException {
        try {
            final Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
            return cipher;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            e.printStackTrace();
            throw new BiometricException(
                    "Can not get instance of Cipher object" + e.getMessage(),
                    BiometricErrorCodes.ERROR_GET_CIPHER_INSTANCE
            );
        }
    }

    // => 4
    private void initEncodeCipher(Cipher cipher, String alias, KeyStore keyStore)
            throws BiometricException {
        try {
            final PublicKey key = keyStore.getCertificate(alias).getPublicKey();
            final PublicKey unrestricted = KeyFactory.getInstance(key.getAlgorithm()).generatePublic(
                    new X509EncodedKeySpec(key.getEncoded()));
            final OAEPParameterSpec spec = new OAEPParameterSpec("SHA-256", "MGF1",
                    MGF1ParameterSpec.SHA1, PSource.PSpecified.DEFAULT);
            cipher.init(Cipher.ENCRYPT_MODE, unrestricted, spec);

        } catch (KeyStoreException | InvalidKeySpecException |
                NoSuchAlgorithmException | InvalidKeyException |
                InvalidAlgorithmParameterException e) {
            throw new BiometricException(
                    "Can not initialize Encode Cipher:" + e.getMessage(),
                    BiometricErrorCodes.ERROR_INIT_ENDECODE_CIPHER
            );
        }
    }

    // => 5
    // More information about this hack
    // from https://developer.android.com/reference/android/security/keystore/KeyGenParameterSpec.html
    // from https://code.google.com/p/android/issues/detail?id=197719
    private Cipher getEncodeCipher(@NonNull Context context, String alias, boolean isAuthenticationRequired)
            throws BiometricException {
        final Cipher cipher = getCipherInstance();
        final KeyStore keyStore = loadKeyStore();
        generateKeyIfNecessary( alias, keyStore, isAuthenticationRequired);
        initEncodeCipher(cipher, alias, keyStore);
        return cipher;
    }

    // => 7
    // Save result to sharedPreferences
    @Override
    public String encode(@NonNull Context context, String alias, String input, boolean isAuthorizationRequired) throws BiometricException {
        try {
            final Cipher cipher = getEncodeCipher(context, alias, isAuthorizationRequired);
            final byte[] bytes = cipher.doFinal(input.getBytes());
            return Base64.encodeToString(bytes, Base64.NO_WRAP);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
            throw new BiometricException(
                    "Error while encoding : " + e.getMessage(),
                    BiometricErrorCodes.ERROR_ENCODING
            );
        }
    }

    // Init decoding cipher
    private void initDecodeCipher(Cipher cipher, String alias) throws BiometricException {
        try {
            final KeyStore keyStore = loadKeyStore();
            final PrivateKey key  = (PrivateKey) keyStore.getKey(alias, null);
            cipher.init(Cipher.DECRYPT_MODE, key);

        } catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableKeyException
                | InvalidKeyException e) {
            e.printStackTrace();
            throw  new BiometricException(
                    "Error init decode Cipher: " + e.getMessage(),
                    BiometricErrorCodes.ERROR_INIT_DECODE_CIPHER
            );
        }

    }

    // Decode public key
    @Override
    public String decode(String alias, String encodedString) throws BiometricException  {
        try {
            final Cipher cipher = getCipherInstance();
            initDecodeCipher(cipher, alias);
            final byte[] bytes = Base64.decode(encodedString, Base64.NO_WRAP);
            return new String(cipher.doFinal(bytes));
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
            throw  new BiometricException(
                    "Error while decoding: " + e.getMessage(),
                    BiometricErrorCodes.ERROR_DEENCODING
            );
        }
    }

    private String decode(String encodedString, Cipher cipher) throws BiometricException  {
        try {
            final byte[] bytes = Base64.decode(encodedString, Base64.NO_WRAP);
            return new String(cipher.doFinal(bytes));
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
            throw  new BiometricException(
                    "Error while decoding: " + e.getMessage(),
                    BiometricErrorCodes.ERROR_DEENCODING
            );
        }
    }


    // Key exists
    @Override
    public boolean isKeystoreContainAlias(String alias) throws BiometricException {
        final KeyStore keyStore = loadKeyStore();
        try {
            return keyStore.containsAlias(alias);
        } catch (KeyStoreException e) {
            e.printStackTrace();
            throw new BiometricException(
                    e.getMessage(),
                    BiometricErrorCodes.ERROR_KEY_STORE
            );
        }
    }


    // Delete a key-value entry
    @Override
    public void deleteKey(String alias) throws BiometricException {
        final KeyStore keyStore = loadKeyStore();
        try {
            keyStore.deleteEntry(alias);
        } catch (KeyStoreException e) {
            e.printStackTrace();
            throw new BiometricException(
                    "Can not delete key: " + e.getMessage(),
                    BiometricErrorCodes.ERROR_DELETE_KEY
            );
        }
    }

}
