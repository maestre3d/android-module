/*
 * Autor: Alonso Ruiz
 * Versión: 0.5b;
 * Descripción: Clase controlador para la actividad PreOrder.
 * Fecha: 02/10/2019
 * */

package com.damascus.atomus;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.damascus.atomus.Models.Order;
import com.damascus.atomus.Models.Practice;
import com.damascus.atomus.Services.Global;
import com.damascus.atomus.Services.UserService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Response;

public class PreorderActivity extends AppCompatActivity implements View.OnClickListener {

    // Current practice
    private static Practice practice;
    // Gson object
    Gson gson = new Gson();

    // Time formatter stuff
    SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMMM hh:mm a", Locale.getDefault());
    java.util.Date time;

    // UI expressions
    TextView coursename, practicename, teachername, expdate, labname;
    ListView materialView;
    Button post, files;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.slidein_right, R.anim.slideout_left);

        setContentView(R.layout.activity_preorder);


        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);


        // Component loader
        ActionBar actionBar = getSupportActionBar();
        coursename = (TextView) findViewById(R.id.preorder_course);
        practicename = (TextView) findViewById(R.id.preorder_practice);
        teachername = (TextView) findViewById(R.id.preorder_docent);
        expdate = (TextView) findViewById(R.id.preorder_expdate);
        labname = (TextView) findViewById(R.id.preorder_lab);

        post = (Button) findViewById(R.id.preorder_post);
        files = (Button) findViewById(R.id.preorder_adj);

        materialView = (ListView) findViewById(R.id.preorder_list);

        // Set buttons listeners
        files.setOnClickListener(this);
        post.setOnClickListener(this);

        // Create listview adapter
        ArrayAdapter adapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_2, android.R.id.text1, practice.getItems()) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                TextView text2 = (TextView) view.findViewById(android.R.id.text2);

                text2.setTextSize(12);
                text2.setTextColor(Color.GRAY);

                if(practice.getItems().get(position).getMaterial() != null){
                    text1.setText(practice.getItems().get(position).getMaterial().getName());
                    text2.setText("Cantidad: " + practice.getItems().get(position).getQuantity().toString());
                }
                return view;
            }
        };

        //  Set listview adapter and item listener
        materialView.setAdapter(adapter);
        materialView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView args0, View view, int position, long id) {
                Toast.makeText(PreorderActivity.this, practice.getItems().get(position).getMaterial().getName(), Toast.LENGTH_SHORT).show();
            }
        });

        // Load UI/UX data
        time = new java.util.Date((long)practice.getExpDate()*1000);

        actionBar.setTitle(practice.getName());
        coursename.setText(practice.getCourse().getName());
        practicename.setText(practice.getName());
        teachername.setText("Maestro(a): " + practice.getCourse().getTeacher().getName() + " " + practice.getCourse().getTeacher().getSurname());
        labname.setText(practice.getCourse().getLab().getName());
        expdate.setText(dateFormat.format(time));


    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        this.finish();
        overridePendingTransition(R.anim.slidein_left, R.anim.slideout_right);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        overridePendingTransition(R.anim.slidein_left, R.anim.slideout_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Buttons listeners
    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.preorder_adj:
                if (practice.getFile() != null){
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Global.FILE_URI + practice.getFile()));
                    startActivity(browserIntent);
                }else{
                    Toast.makeText(PreorderActivity.this, "La práctica no contiene archivo.",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.preorder_post:
                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                    builder.setMessage(Html.fromHtml( getString(R.string.infoAlert), Html.FROM_HTML_MODE_LEGACY))
                            .setTitle("¿Desea completar la orden?")
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if(practice.getItems() != null){
                                        if(practice.getItems().size() > 0){
                                            UserService userService = new UserService();
                                            userService.postOrder( getWindow().getDecorView().getRootView(), practice.getId());

                                        }else {
                                            Toast.makeText(getBaseContext(), "Práctica sin material", Toast.LENGTH_SHORT).show();
                                        }
                                    }else{
                                        Toast.makeText(getBaseContext(), "Práctica sin material", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            })
                            .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            }).show();
                }else{
                    builder.setMessage("Para confirmar su orden, favor de presionar el botón Aceptar.\n\n" + Html.fromHtml(getString(R.string.infoAlert)))
                            .setTitle("¿Desea completar la orden?")
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if(practice.getItems() != null){
                                        if(practice.getItems().size() > 0){
                                            UserService userService = new UserService();
                                            userService.postOrder( getWindow().getDecorView().getRootView(), practice.getId());

                                        }else {
                                            Toast.makeText(getBaseContext(), "Práctica sin material", Toast.LENGTH_SHORT).show();
                                        }
                                    }else{
                                        Toast.makeText(getBaseContext(), "Práctica sin material", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            })
                            .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            }).show();
                }

                break;
        }
    }


    // Current practice setter
    public void setPractice(Practice practice) {
        this.practice = practice;
    }

    // Display confirmed order
    public void setOrder(View v, Response<JsonObject> response){
        Order order = null;
        if(response.isSuccessful()){
            //order = gson.fromJson(response.body().toString(), Order.class);
            Toast.makeText(v.getContext(), "Orden confirmada", Toast.LENGTH_SHORT).show();
        }else{
            try {
                JSONObject res = new JSONObject(response.errorBody().string());
                AlertDialog.Builder builder = new AlertDialog.Builder( v.getContext() , R.style.MyDialogTheme);
                builder.setMessage(res.getString("message"))
                        .setTitle("Error al ordenar")
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).show();
            } catch (JSONException e) {
            } catch (IOException e) {
            }
        }
    }

}
