package com.damascus.atomus.Biometrics;

import android.content.Context;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;

import com.damascus.atomus.Biometrics.Callbacks.HelperCallback;

public class FingerprintHelper implements BiometricHelper {


    private static final String FINGERPRINT_ALIAS = "fp_fingerprint_lock_screen_key_store";
    private static final String PIN_ALIAS = "fp_pin_lock_screen_key_store";

    private static final FingerprintHelper ourInstance = new FingerprintHelper();

    public static FingerprintHelper getInstance() {
        return ourInstance;
    }

    private final BiometricUtils pfSecurityUtils
            = BiometricFactory.getPFSecurityUtilsInstance();

    private FingerprintHelper() {

    }

    @Override
    public void encodePin(Context context, String pin, HelperCallback<String> callback) {
        try {
            final String encoded = pfSecurityUtils.encode(context, PIN_ALIAS, pin, false);
            if (callback != null) {
                callback.onResult(new BiometricResult<>(encoded));
            }
        } catch (BiometricException e) {
            if (callback != null) {
                callback.onResult(new BiometricResult<>(e.getError()));
            }
        }
    }




    private boolean isFingerPrintAvailable(Context context) {
        return FingerprintManagerCompat.from(context).isHardwareDetected();
    }

    private boolean isFingerPrintReady(Context context) {
        return FingerprintManagerCompat.from(context).hasEnrolledFingerprints();
    }

    @Override
    public void delete(HelperCallback<Boolean> callback) {
        try {
            pfSecurityUtils.deleteKey(PIN_ALIAS);
            if (callback != null) {
                callback.onResult(new BiometricResult<>(true));
            }
        } catch (BiometricException e) {
            if (callback != null) {
                callback.onResult(new BiometricResult<>(e.getError()));
            }
        }
    }


    @Override
    public void isPinCodeEncryptionKeyExist(HelperCallback<Boolean> callback) {
        try {
            final boolean isExist = pfSecurityUtils.isKeystoreContainAlias(PIN_ALIAS);
            if (callback != null) {
                callback.onResult(new BiometricResult<>(isExist));
            }
        } catch (BiometricException e) {
            if (callback != null) {
                callback.onResult(new BiometricResult<>(e.getError()));
            }
        }
    }

}
