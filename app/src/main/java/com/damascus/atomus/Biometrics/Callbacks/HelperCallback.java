package com.damascus.atomus.Biometrics.Callbacks;

import com.damascus.atomus.Biometrics.BiometricResult;

public interface HelperCallback<T> {
    void onResult(BiometricResult<T> result);
}
