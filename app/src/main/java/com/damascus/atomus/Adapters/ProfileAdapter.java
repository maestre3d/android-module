/*
 * Autor: Alonso Ruiz
 * Versión: 0.5b;
 * Descripción: Clase adaptador para la creación del RecyclerView de Profile.
 * Fecha: 02/10/2019
 * */

package com.damascus.atomus.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.damascus.atomus.Models.Course;
import com.damascus.atomus.R;

import java.util.List;

public class ProfileAdapter extends
        RecyclerView.Adapter<ProfileAdapter.ViewHolder> {

    // Construct, set all Resources from item view
    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView courseName, docentName, courseGrade, labName;

        public ViewHolder(View itemView){
            super(itemView);

            courseName = (TextView) itemView.findViewById(R.id.adpro_course);
            docentName = (TextView) itemView.findViewById(R.id.adpro_doname);
            courseGrade = (TextView) itemView.findViewById(R.id.adpro_cograde);
            labName = (TextView) itemView.findViewById(R.id.adpro_dolab);
        }
    }

    // List for position onBind
    private List<Course> mCourses;

    // Set list
    public ProfileAdapter(List<Course> courses) { mCourses = courses; }

    @Override
    public ProfileAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        // Get context from activity
        Context context = parent.getContext();
        // Inflate layout from context
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate item on activity
        View courseView = inflater.inflate(R.layout.profile_item, parent, false);

        // Create viewholder when inflate
        ViewHolder viewHolder = new ViewHolder(courseView);
        return viewHolder;
    }

    // When viewHolder gets bind, set values on ItemView
    @Override
    public void onBindViewHolder(ProfileAdapter.ViewHolder viewHolder, int position){
        // Load the model
        final Course course = mCourses.get(position);

        // Declare Resources
        TextView courseName = viewHolder.courseName;
        TextView docentName = viewHolder.docentName;
        TextView courseGrade = viewHolder.courseGrade;
        TextView labName = viewHolder.labName;

        // Set values
        courseName.setText(course.getName());
        docentName.setText(course.getTeacher().getName() + " " + course.getTeacher().getSurname());
        courseGrade.setText(course.getGrade().toString() + " Semestre");
        if(course.getLab() != null){
            labName.setText(course.getLab().getName());
        }


    }

    // Set list size
    @Override
    public int getItemCount(){ return mCourses.size(); }
}
