package com.damascus.atomus.Remote;

import com.damascus.atomus.Services.Global;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface APIService {

    // Login -> Hash retrieved.
    @POST(ApiConstants.URL_LOGIN)
    @FormUrlEncoded
    void logIn(@Field("username") String username,
               @Field("password") String password,
               @Field("gethash") String token,
               Callback<JsonObject> serverResponse);

    @POST(ApiConstants.URL_DEVICE)
    @FormUrlEncoded
    Observable<Response<JsonObject>> setDevice(@Field("deviceId") String deviceId,
                                               @Header("Authorization") String token);
    @GET(ApiConstants.URL_DEVICE)
    Observable<Response<JsonObject>> unsetDevice(@Header("Authorization") String token);

    @POST(ApiConstants.URL_LOGIN)
    @FormUrlEncoded
    Observable<Response<JsonObject>> logIn(@Field("username")   String username,
                               @Field("password")   String password,
                               @Field("gethash")    String token);

    @POST(ApiConstants.URL_ORDER)
    @FormUrlEncoded
    Observable<Response<JsonObject>> makeOrder(@Field("practice") String practice,
                                               @Header("Authorization") String token);
    @GET(ApiConstants.URL_ORDER + "/{Id}")
    Observable<Response<JsonObject>> getOrder(@Path("Id") String orderId,
                                              @Header("Authorization") String token);
    @DELETE(ApiConstants.URL_ORDER + "/{Id}")
    Observable<Response<JsonObject>> removeOrder(@Path("Id") String orderId,
                                                 @Header("Authorization") String token);
    @GET(ApiConstants.URL_ACTIVE)
    Observable<Response<JsonArray>> getActiveOrders(@Header("Authorization") String token);

    @GET(ApiConstants.URL_EXPIRED)
    Observable<Response<JsonArray>> getExpOrders(@Header("Authorization") String token);

    @GET( ApiConstants.URL_USER + "{Id}")
    Observable<Response<JsonObject>> getUser(@Path("Id") String userId, @Header("Authorization") String token);

    @GET( ApiConstants.URL_HOME )
    Observable<Response<JsonArray>> getFeed(@Header("Authorization") String token);

    @GET("user")
    Call<JsonArray> getUsers(@Header("Authorization") String token);
}
