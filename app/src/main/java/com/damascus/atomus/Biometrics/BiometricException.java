package com.damascus.atomus.Biometrics;

public class BiometricException extends Exception {

    private final Integer mCode;

    public BiometricException(String message, Integer code) {
        super(message);
        mCode = code;
    }

    public Integer getCode() {
        return mCode;
    }

    public BiometricError getError() {
        return new BiometricError(getMessage(), getCode());
    }
}