/*
 * Autor: Alonso Ruiz
 * Versión: 0.5b;
 * Descripción: Clase controlador para la actividad Log.
 * Fecha: 02/10/2019
 * */

package com.damascus.atomus;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.design.widget.TextInputLayout;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.support.v4.os.CancellationSignal;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.damascus.atomus.Biometrics.BiometricManager;
import com.damascus.atomus.Biometrics.BiometricUtils;
import com.damascus.atomus.Biometrics.Callbacks.AuthCallback;
import com.damascus.atomus.Remote.RetrofitClient;
import com.damascus.atomus.Services.UserService;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;


public class LogActivity extends AppCompatActivity   {
    ProgressBar spinner;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Vibrator vibrator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);
        CustomGlobal.isBooted = false;
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        Button submitBtn = findViewById(R.id.log_btn);
        Button fingerBtn = findViewById(R.id.log_finger);
        final TextInputLayout username = (TextInputLayout) findViewById(R.id.log_user);
        final TextInputLayout password = (TextInputLayout) findViewById(R.id.log_pass);
        spinner = findViewById(R.id.log_spin);
        preferences = getApplicationContext().getSharedPreferences("Credentials", 0);
        editor = preferences.edit();

        fingerBtn.setVisibility(View.GONE);

        /*if(BiometricUtils.isFingerprintAuthAvailable(this)){
            fingerBtn.setVisibility(View.VISIBLE);
            fingerBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    makeVibration(400);

                    Toast.makeText(LogActivity.this, "Finger Init", Toast.LENGTH_SHORT).show();
                }
            });
        }*/

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLog(username.getEditText().getText().toString(), password.getEditText().getText().toString());
            }
        });

    }

    private void getLog(String x, String y){

        spinner.setVisibility(View.VISIBLE);

        RetrofitClient.logInUser(x, y)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(e -> {
                    spinner.setVisibility(View.GONE);
                })
                .onErrorReturn(ex ->{
                    JSONObject message;
                    Response<JsonObject> response = Response.error(500,
                            ResponseBody.create(
                                MediaType.parse("application/json"),
                                "{\"message\":\"No se pudo conectar al servidor.\"}"
                            ));
                    message = new JSONObject(response.errorBody().string());
                    Toast.makeText(LogActivity.this, message.getString("message"), Toast.LENGTH_SHORT).show();
                    return response;
                })
                .subscribe(logResponse -> {
                    UserService userService = new UserService();
                    boolean flag = userService.setUser(logResponse, LogActivity.this, editor);
                    spinner.setVisibility(View.GONE);
                    if(flag == true){
                        Intent intent = new Intent(LogActivity.this, MainActivity.class );
                        startActivity(intent);
                    }else{
                        makeVibration(250);
                    }
                });
    }


    private void makeVibration(int ms){
        // Vibrate for X milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createOneShot(ms, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            vibrator.vibrate(ms);
        }
    }

    public void startListening(FingerprintManagerCompat.CryptoObject cryptoObject) {
        if (!BiometricUtils.isFingerprintAuthAvailable(LogActivity.this)) {
            return;
        }
        FingerprintManagerCompat manager = FingerprintManagerCompat.from(LogActivity.this);
        CancellationSignal cancellationSignal = new CancellationSignal();
        AuthCallback authCallback = new AuthCallback();
        manager.authenticate(
                cryptoObject, 0, cancellationSignal, authCallback, null);
    }
}
