/*
 * Autor: Alonso Ruiz
 * Versión: 0.5b;
 * Descripción: Clase modelo para la colección Course.
 * Fecha: 02/10/2019
 * */
package com.damascus.atomus.Models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Course {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("grade")
    @Expose
    private Integer grade;
    @SerializedName("topic")
    @Expose
    private String topic;
    @SerializedName("teacher")
    @Expose
    private Teacher teacher;
    @SerializedName("lab")
    @Expose
    private Lab lab;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Lab getLab() {
        return lab;
    }

    public void setLab(Lab lab) {
        this.lab = lab;
    }
}
