package com.damascus.atomus.Biometrics;

public class BiometricResult<T> {
    private BiometricError mError = null;
    private T mResult = null;

    public BiometricResult(BiometricError mError) {
        this.mError = mError;
    }

    public BiometricResult(T result) {
        mResult = result;
    }

    public BiometricError getError() {
        return mError;
    }

    public T getResult() {
        return mResult;
    }
}
