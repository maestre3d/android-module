/*
 * Autor: Alonso Ruiz
 * Versión: 0.5b;
 * Descripción: Clase controlador para la actividad Main.
 * Fecha: 02/10/2019
 * */

package com.damascus.atomus;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.damascus.atomus.Services.Global;
import com.damascus.atomus.Services.UserService;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;


public class MainActivity extends AppCompatActivity {

    // Services
    UserService userService;
    // Navigation
    BottomNavigationView navigation;

    // Fragments
    HomeFragment homeFragment;
    OrderFragment orderFragment;
    ProfileFragment profileFragment;
    CalendarFragment calendarFragment;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    CustomGlobal.window = "HOME";
                    setFragment(homeFragment);
                    return true;

                case R.id.navigation_orders:
                    CustomGlobal.window = "ORDERS";
                    setFragment(orderFragment);
                    return true;

                case R.id.navigation_calendar:
                    CustomGlobal.window = "CALENDAR";
                    setFragment(calendarFragment);
                    return true;

                case R.id.navigation_profile:
                    CustomGlobal.window = "PROFILE";
                    setFragment(profileFragment);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        setContentView(R.layout.activity_main);


        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        FirebaseApp.initializeApp(this);


        // Fragments init
        homeFragment = new HomeFragment();
        orderFragment = new OrderFragment();
        profileFragment = new ProfileFragment();
        calendarFragment = new CalendarFragment();

        // Get user's courses
        userService = new UserService();
        userService.refreshIdentity(getWindow().getDecorView().getRootView());
        userService.refreshFeed(getWindow().getDecorView().getRootView());
        userService.getActiveOrd(getWindow().getDecorView().getRootView());
        userService.getExpOrders(getWindow().getDecorView().getRootView());

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            System.out.println("getInstanceId failed");
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        if(Global.IDENTITY.getDeviceId() != token){
                            Global.IDENTITY.setDeviceId(token);
                            userService.setDeviceFCM();
                        }
                    }
                });
    }

    @Override
    public void onResume(){
        super.onResume();
        // Set HOME when logsIn
        if (CustomGlobal.window == "HOME"){
            navigation.setSelectedItemId(R.id.navigation_home);
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();

        // Set HOME when logOut is pressed
        if (CustomGlobal.isHome == true){
            CustomGlobal.window = "HOME";
        }
    }

    @Override
    public void onBackPressed(){
        if(CustomGlobal.window == "HOME"){
            moveTaskToBack(true);
        }else{
            navigation.setSelectedItemId(R.id.navigation_home);
        }
    }

    private void setFragment(Fragment fragment){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container_main, fragment);
        fragmentTransaction.commit();
    }
}
