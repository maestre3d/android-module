package com.damascus.atomus.Services;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.damascus.atomus.Adapters.EmptyRecyclerView;
import com.damascus.atomus.Adapters.HomeAdapter;
import com.damascus.atomus.CustomGlobal;
import com.damascus.atomus.HomeFragment;
import com.damascus.atomus.Models.Order;
import com.damascus.atomus.Models.Practice;
import com.damascus.atomus.Models.User;
import com.damascus.atomus.OrderActivity;
import com.damascus.atomus.OrderFragment;
import com.damascus.atomus.PreorderActivity;
import com.damascus.atomus.R;
import com.damascus.atomus.Remote.APIService;
import com.damascus.atomus.Remote.RetrofitClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class UserService {
    Gson gson = new Gson();

    public boolean setUser(Response<JsonObject> response, Context context, SharedPreferences.Editor editor){
        try {
            FirebaseApp.initializeApp(context);
            JSONObject res;
            if(response.isSuccessful()){
                res = new JSONObject(response.body().toString());
                String token = res.getString("token");
                JwtService jwtService = new JwtService(token);
                jwtService.storeCreds(editor);

                FirebaseInstanceId.getInstance().getInstanceId()
                        .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                            @Override
                            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                if (!task.isSuccessful()) {
                                    System.out.println("getInstanceId failed");
                                    return;
                                }

                                // Get new Instance ID token
                                String token = task.getResult().getToken();
                                Global.IDENTITY.setDeviceId(token);
                                //System.out.println(Global.IDENTITY.getDeviceId());
                                setDeviceFCM();

                                // Log and toast
                                //System.out.println(msg);
                                //Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                            }
                        });
                return true;
            }else{
                res = new JSONObject(response.errorBody().string());
                String message = res.getString("message");
                if(response.code() == 403){
                    AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.MyDialogTheme);

                    builder.setMessage(message)
                            .setTitle("Problema con el usuario")
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }else{
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        } catch (JSONException e) {
            return false;
        } catch (IOException e) {
            return false;
        }
    }

    public void refreshIdentity(View v){
        RetrofitClient.getUser(Global.IDENTITY.get_id(), Global.TOKEN)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(e -> {
                })
                .onErrorReturn(ex ->{
                    JSONObject message;
                    Response<JsonObject> response = Response.error(500,
                            ResponseBody.create(
                                    MediaType.parse("application/json"),
                                    "{\"message\":\"No se pudo conectar al servidor.\"}"
                            ));
                    message = new JSONObject(response.errorBody().string());
                    System.out.println(ex.getMessage());
                    Toast.makeText(v.getContext(), message.getString("message"), Toast.LENGTH_SHORT).show();
                    return response;
                })
                .subscribe(Response -> {
                    UserService userService = new UserService();
                    userService.setIdentity(Response);
                    CustomGlobal.isCourse = true;
                });
    }

    public void setIdentity(Response<JsonObject> response){
        if(response.isSuccessful()){
            User user;
            user = gson.fromJson(response.body().toString(), User.class);
            Global.IDENTITY = user;
        }else{
            try {
                JSONObject res = new JSONObject(response.errorBody().string());
                System.out.println(res.getString("message"));
            } catch (JSONException e) {
            } catch (IOException e) {
            }
        }
    }

    public void refreshFeed(View v){
        RetrofitClient.getFeed(Global.TOKEN)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(e -> {
                })
                .onErrorReturn(ex ->{
                    JSONObject message;
                    Response<JsonArray> response = Response.error(500,
                            ResponseBody.create(
                                    MediaType.parse("application/json"),
                                    "{\"message\":\"No se pudo conectar al servidor.\"}"
                            ));
                    message = new JSONObject(response.errorBody().string());
                    System.out.println(ex.getMessage());
                    Toast.makeText(v.getContext(), message.getString("message"), Toast.LENGTH_SHORT).show();
                    return response;
                })
                .subscribe(Response -> {
                    Animation animation = AnimationUtils.loadAnimation(v.getContext(), R.anim.fade_in);
                    EmptyRecyclerView rvPractices = (EmptyRecyclerView) v.findViewById(R.id.home_cards);
                    TextView empty_view = (TextView) v.findViewById(R.id.home_nodata);
                    LinearLayout holder = (LinearLayout) v.findViewById(R.id.holder_h);
                    SwipeRefreshLayout mySwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.frame_home);

                    HomeFragment homeFragment = new HomeFragment();
                    homeFragment.setPractices(setFeed(Response));

                    if (setFeed(Response) != null){
                        if(rvPractices != null){
                            HomeAdapter adapter = new HomeAdapter(setFeed(Response));
                            rvPractices.setAdapter(adapter);
                            rvPractices.setAnimation(animation);
                            empty_view.setVisibility(View.GONE);
                            holder.setVisibility(View.GONE);

                            if (mySwipeRefreshLayout.isRefreshing()) {
                                mySwipeRefreshLayout.setRefreshing(false);
                            }
                        }
                    }else{
                        holder.setVisibility(View.GONE);
                        empty_view.setVisibility(View.VISIBLE);
                        rvPractices.setEmptyView(empty_view);
                        rvPractices.setAnimation(animation);

                        if (mySwipeRefreshLayout.isRefreshing()) {
                            mySwipeRefreshLayout.setRefreshing(false);
                        }
                    }
                });
    }

    public ArrayList<Practice> setFeed(Response<JsonArray> response){
        if(response.isSuccessful()){
            ArrayList<Practice> feed = new ArrayList<Practice>();
            if(response.body().size() > 0){
                for (int i = 0; i < response.body().size(); i++){
                    feed.add(gson.fromJson(response.body().get(i).toString(), Practice.class));
                }
                return feed;
            }else{
                return null;
            }

        }else{
            return null;
        }
    }

    public void postOrder(View v, String practice_id){
        RetrofitClient.makeOrder(practice_id, Global.TOKEN)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(e -> {
                })
                .onErrorReturn(ex ->{
                    JSONObject message;
                    Response<JsonObject> response = Response.error(500,
                            ResponseBody.create(
                                    MediaType.parse("application/json"),
                                    "{\"message\":\"No se pudo conectar al servidor.\"}"
                            ));
                    message = new JSONObject(response.errorBody().string());
                    System.out.println(ex.getMessage());
                    Toast.makeText(v.getContext(), message.getString("message"), Toast.LENGTH_SHORT).show();
                    return response;
                })
                .subscribe(Response -> {
                    PreorderActivity preorderActivity = new PreorderActivity();
                    preorderActivity.setOrder(v, Response);
                });
    }

    public void getActiveOrd(View v){
        RetrofitClient.getActive(Global.TOKEN)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(e -> {
                })
                .onErrorReturn(ex ->{
                    JSONObject message;
                    Response<JsonArray> response = Response.error(500,
                            ResponseBody.create(
                                    MediaType.parse("application/json"),
                                    "{\"message\":\"No se pudo conectar al servidor.\"}"
                            ));
                    message = new JSONObject(response.errorBody().string());
                    System.out.println(ex.getMessage());
                    Toast.makeText(v.getContext(), message.getString("message"), Toast.LENGTH_SHORT).show();
                    return response;
                })
                .subscribe(Response -> {
                    ArrayList<Order> orders = setOrders(Response);
                    if ( orders != null){
                        CustomGlobal.orders = orders;
                    }else{
                        CustomGlobal.orders.clear();
                    }
                });
    }

    public ArrayList<Order> setOrders(Response<JsonArray> response){
        if(response.isSuccessful()){
            ArrayList<Order> orders = new ArrayList<Order>();
            if(response.body().size() > 0){
                for (int i = 0; i < response.body().size(); i++){
                    orders.add(gson.fromJson(response.body().get(i).toString(), Order.class));
                }
                return orders;
            }else{
                return null;
            }

        }else{
            return null;
        }
    }

    public void getExpOrders(View v){
        RetrofitClient.getExp(Global.TOKEN)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(e -> {
                })
                .onErrorReturn(ex ->{
                    JSONObject message;
                    Response<JsonArray> response = Response.error(500,
                            ResponseBody.create(
                                    MediaType.parse("application/json"),
                                    "{\"message\":\"No se pudo conectar al servidor.\"}"
                            ));
                    message = new JSONObject(response.errorBody().string());
                    System.out.println(ex.getMessage());
                    Toast.makeText(v.getContext(), message.getString("message"), Toast.LENGTH_SHORT).show();
                    return response;
                })
                .subscribe(Response -> {
                    ArrayList<Order> orders = setOrders(Response);
                    if ( orders != null){
                        CustomGlobal.exp_ords = orders;
                    }else{
                        CustomGlobal.exp_ords.clear();
                    }
                });
    }

    public void rmOrder(View v, String id){
        RetrofitClient.removeOrder(id, Global.TOKEN)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(e -> { })
                .onErrorReturn(ex -> {
                    JSONObject message;
                    Response<JsonObject> response = Response.error(500,
                            ResponseBody.create(
                                    MediaType.parse("application/json"),
                                    "{\"message\":\"No se pudo conectar al servidor.\"}"
                            ));
                    message = new JSONObject(response.errorBody().string());
                    System.out.println(ex.getMessage());
                    Toast.makeText(v.getContext(), message.getString("message"), Toast.LENGTH_SHORT).show();
                    return response;
                })
                .subscribe(response -> {
                    if(response.isSuccessful()){
                        Toast.makeText(v.getContext(), "Orden cancelada correctamente.", Toast.LENGTH_SHORT).show();
                    }else{
                        JSONObject message = new JSONObject(response.errorBody().string());
                        Toast.makeText(v.getContext(), message.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void setDeviceFCM(){
        if(Global.IDENTITY.getDeviceId() != null){
            RetrofitClient.setDevice(Global.IDENTITY.getDeviceId(), Global.TOKEN)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnError(e -> {
                    })
                    .onErrorReturn(ex ->{
                        JSONObject message;
                        Response<JsonObject> response = Response.error(500,
                                ResponseBody.create(
                                        MediaType.parse("application/json"),
                                        "{\"message\":\"No se pudo conectar al servidor.\"}"
                                ));
                        message = new JSONObject(response.errorBody().string());
                        System.out.println(ex.getMessage());
                        System.out.println(message.getString("message"));
                        return response;
                    })
                    .subscribe(Response -> {
                        if(Response.isSuccessful()){
                            JSONObject message = new JSONObject(Response.body().toString());
                            System.out.println(message.getString("message"));
                        }else{
                            System.out.println("FCM Device -> FATAL ERROR");
                        }
                    });
        }
    }

    public void unsetDeviceFCM(){
        if(Global.IDENTITY.getDeviceId() != null){
            RetrofitClient.unsetDevice(Global.TOKEN)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnError(e -> {
                    })
                    .onErrorReturn(ex ->{
                        JSONObject message;
                        Response<JsonObject> response = Response.error(500,
                                ResponseBody.create(
                                        MediaType.parse("application/json"),
                                        "{\"message\":\"No se pudo conectar al servidor.\"}"
                                ));
                        message = new JSONObject(response.errorBody().string());
                        System.out.println(ex.getMessage());
                        System.out.println(message.getString("message"));
                        return response;
                    })
                    .subscribe(Response -> {
                        if(Response.isSuccessful()){
                            JSONObject message = new JSONObject(Response.body().toString());
                            System.out.println(message.getString("message"));
                        }else{
                            System.out.println("FCM Device -> FATAL ERROR");
                        }
                    });
        }
    }


}
