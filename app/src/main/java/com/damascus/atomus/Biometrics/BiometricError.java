package com.damascus.atomus.Biometrics;


public class BiometricError {

    private final String mMessage;
    private final Integer mCode;

    BiometricError(String message, Integer code) {
        mMessage = message;
        mCode = code;
    }

    public String getMessage() {
        return mMessage;
    }

    public Integer getCode() {
        return mCode;
    }
}
