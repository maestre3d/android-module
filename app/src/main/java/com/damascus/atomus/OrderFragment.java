/*
 * Autor: Alonso Ruiz
 * Versión: 0.5b;
 * Descripción: Clase controlador para el fragmento Order.
 * Fecha: 02/10/2019
 * */

package com.damascus.atomus;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


import com.damascus.atomus.Models.Order;
import com.damascus.atomus.Services.UserService;

import java.util.ArrayList;


public class OrderFragment extends Fragment {

    public ListView listView;
    ListView listView2;
    LinearLayout holder1, holder2;
    ConstraintLayout container1, container2;
    UserService userService = new UserService();
    View vv;
    ArrayAdapter exp_adapter;
    ArrayAdapter adapter;




    public OrderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Set window name
        CustomGlobal.window = "ORDER";

        // Get View to return and inflate
        View v = inflater.inflate(R.layout.fragment_order, container, false);
        vv = v;

        userService.getActiveOrd(v);
        userService.getExpOrders(v);



        // Get listviews from R
        listView = (ListView) v.findViewById(R.id.order_list);
        listView2 = (ListView) v.findViewById(R.id.order_list2);
        container1 = (ConstraintLayout) v.findViewById(R.id.order_container1);
        container2 = (ConstraintLayout) v.findViewById(R.id.order_container2);
        holder1 = (LinearLayout) v.findViewById(R.id.order_holder1);
        holder2 = (LinearLayout) v.findViewById(R.id.order_holder2);
        holder1.setVisibility(View.VISIBLE);
        holder2.setVisibility(View.VISIBLE);
        container1.setVisibility(View.GONE);
        container2.setVisibility(View.GONE);



        // Set adapters to listviews
        if(CustomGlobal.orders.size() > 0){
            holder1.setVisibility(View.GONE);
            container1.setVisibility(View.VISIBLE);
            // Insert values and create adapter with android default list layout
            adapter = new ArrayAdapter(v.getContext(), android.R.layout.simple_list_item_2, android.R.id.text1, CustomGlobal.orders) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View view = super.getView(position, convertView, parent);
                    TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                    TextView text2 = (TextView) view.findViewById(android.R.id.text2);

                    text2.setTextSize(12);
                    text2.setTextColor(Color.GRAY);

                    text1.setText(CustomGlobal.orders.get(position).getPractice().getName());
                    text2.setText("Status: " + CustomGlobal.orders.get(position).getStatus());

                    return view;
                }
            };
            listView.setAdapter(adapter);
        }

        if(CustomGlobal.exp_ords.size() > 0){
            holder2.setVisibility(View.GONE);
            container2.setVisibility(View.VISIBLE);
            exp_adapter = new ArrayAdapter(v.getContext(), android.R.layout.simple_list_item_2, android.R.id.text1, CustomGlobal.exp_ords) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View view = super.getView(position, convertView, parent);

                    TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                    TextView text2 = (TextView) view.findViewById(android.R.id.text2);

                    text2.setTextSize(12);
                    text2.setTextColor(Color.GRAY);

                    text1.setText(CustomGlobal.exp_ords.get(position).getPractice().getName());
                    text2.setText("Status: " + CustomGlobal.exp_ords.get(position).getStatus());
                    return view;
                }
            };
            listView2.setAdapter(exp_adapter);
        }

        OrderActivity orderActivity = new OrderActivity();

        // Set listener on 1st listview => NonExpired
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView <?> parent, View view, int position, long id) {
                if(CustomGlobal.orders.size() > 0 && CustomGlobal.orders != null ){
                    orderActivity.setOrder(CustomGlobal.orders.get(position));
                    //Toast.makeText(getContext(), values[position] +"", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(getActivity(), OrderActivity.class);
                    CustomGlobal.isExpired = false;
                    getActivity().startActivity(i);
                }
            }
        });
        // Set listener on 2nd listview => Expired
        listView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView <?> parent, View view, int position, long id) {
                if(CustomGlobal.exp_ords.size() > 0 && CustomGlobal.exp_ords != null){
                    orderActivity.setOrder(CustomGlobal.exp_ords.get(position));
                    Intent i = new Intent(getActivity(), OrderActivity.class);
                    CustomGlobal.isExpired = true;
                    getActivity().startActivity(i);
                }
            }
        });
        // Inflate the layout for this fragment
        return v;
    }

    @Override
    public void onResume(){
        super.onResume();

        if(CustomGlobal.exp_ords.size() > 0){
            exp_adapter.notifyDataSetChanged();
        }

        if(CustomGlobal.orders.size() > 0){
            System.out.println("ON CHANGE ORDER");
            adapter.notifyDataSetChanged();
        }

    }

}
