package com.damascus.atomus.Models;

import com.damascus.atomus.Services.Global;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class User {
    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("surname")
    @Expose
    private String surname;
    @SerializedName("grade")
    @Expose
    private Integer grade;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("course")
    @Expose
    private List<Course> course = null;


    public User() {
    }

    public String get_id() {
        return this._id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return this.name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return this.surname;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Integer getGrade() {
        return this.grade;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return this.email;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<Course> getCourse() {
        return course;
    }

    public void setCourse(List<Course> course) {
        this.course = course;
    }

    public String toString() {
        return (new ToStringBuilder(this)).append("_id", this._id).append("username", this.username).append("password", this.password).append("name", this.name).append("surname", this.surname).append("email", this.email).append("grade", this.grade).append("image", this.image).toString();
    }
}