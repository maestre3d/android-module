package com.damascus.atomus.Services;


import android.content.SharedPreferences;

import com.auth0.android.jwt.Claim;
import com.auth0.android.jwt.JWT;
import com.damascus.atomus.Models.User;

import java.util.Map;

public class JwtService {
    private static JWT identity;

    public JwtService(String token){
        identity = new JWT(token);
        Global.TOKEN = token;
        decodeToken();
    }

    private void decodeToken(){
        User user = new User();
        Map<String, Claim> userMap = identity.getClaims();

        for (Map.Entry<String, Claim> entry : userMap.entrySet()) {
            String k = entry.getKey();
            Claim v = entry.getValue();
            switch (k){
                case "_id":
                    user.set_id(v.asString());
                    break;
                case "name":
                    user.setName(v.asString());
                    break;
                case "surname":
                    user.setSurname(v.asString());
                    break;
                case "username":
                    user.setUsername(v.asString());
                    break;
                case "password":
                    user.setPassword(v.asString());
                    break;
                case "grade":
                    user.setGrade(v.asInt());
                    break;
                case "image":
                    user.setImage(v.asString());
                    break;
                case "email":
                    user.setEmail(v.asString());
                    break;
                case "deviceId":
                    user.setDeviceId(v.asString());
                    break;
                default:
                    break;
            }
        }

        if(user != null){
            Global.IDENTITY = user;
        }
    }

    public void storeCreds(SharedPreferences.Editor editor){
        editor.clear();
        editor.putString("TOKEN", Global.TOKEN );
        editor.commit();
    }

    public boolean checkExp(){
        Long unixTime = System.currentTimeMillis() / 1000L;

        if(identity != null){
            if((identity.getExpiresAt().getTime() / 1000L) > unixTime){
                return true;
            }
            else{ return false; }
        }
        else{ return false; }
    }

}
